export enum StatusOrder {
    CREATED = "created",
    CONFIRMED = "confirmed",
    DELIVERED = "delivered",
    CANCELED = "canceled"
}