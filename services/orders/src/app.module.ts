import { Module } from "@nestjs/common";
import { TypegooseModule } from "nestjs-typegoose";
import { ApiModule } from "./api/api.module";
import { DataModule } from './data/data.module';


@Module({
    imports: [
        TypegooseModule.forRoot("mongodb://mongodb:27017/order", {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }),
        ApiModule,
        DataModule
    ],
})
export class AppModule { }
