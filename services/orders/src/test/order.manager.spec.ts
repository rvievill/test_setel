import { Order } from "src/data/models/order.model";
import { OrderRepository } from "src/data/repositories/orders.repository";
import { StatusOrder } from "../shared/enum/statusOrder.enum";
import { OrderManager } from "../core/orders.manager";

describe("order manager", () => {
    let orderManager: OrderManager = null;

    beforeAll(() => {
        orderManager = new OrderManager(null)
    })

    describe("create", () => {
        it("should have status confirmed", async () => {
            const order: Order = {
                _id: "1",
                name: "coucou",
                status: StatusOrder.CREATED
            }
            const res = orderManager.updateStatus("confirmed", order);
    
            expect(res.status).toEqual(StatusOrder.CONFIRMED)
        })

        it("should have status canceled", async () => {
            const order: Order = {
                _id: "1",
                name: "coucou",
                status: StatusOrder.CREATED
            }
            const res = orderManager.updateStatus("declined", order);
    
            expect(res.status).toEqual(StatusOrder.CANCELED)
        })
    })
})