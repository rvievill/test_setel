import { HttpModule, Module } from '@nestjs/common';

import { DataModule } from "@app/data/data.module";
import { OrderManager } from './orders.manager';


@Module({
    imports: [
        HttpModule,
        DataModule
    ],
    controllers: [],
    providers: [OrderManager],
    exports: [OrderManager]
})
export class CoreModule {}