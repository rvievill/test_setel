import { HttpService, Injectable } from "@nestjs/common";

import { Order } from "@app/data/models/order.model";
import { OrderRepository } from "@app/data/repositories/orders.repository";
import { StatusOrder } from "@app/shared/enum/statusOrder.enum";

@Injectable()
export class OrderManager {
    constructor(
        private orderRepository: OrderRepository
    ) { }

    public async create(order: Order): Promise<Order> {
        return this.orderRepository.create(order);
    }

    public updateStatus(payments: string, order: Order): Order {
        order.status = payments === "confirmed" ? StatusOrder.CONFIRMED : StatusOrder.CANCELED;

        return order;
    }

    public async updateOrder(order: Order): Promise<Order> {
        let orderUpdated = await this.orderRepository.update(order._id, order);

        if (orderUpdated.status === StatusOrder.CONFIRMED) {
            setTimeout(async () => {
                orderUpdated.status = StatusOrder.DELIVERED;
                this.orderRepository.update(order._id, order);
            }, 500);
        }

        return orderUpdated;
    }
}