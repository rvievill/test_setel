import { Injectable } from "@nestjs/common";
import { InjectModel } from "nestjs-typegoose";
import { ReturnModelType } from "@typegoose/typegoose";

import { Order } from "../models/order.model";

@Injectable()
export class OrderRepository {
    constructor(@InjectModel(Order) private readonly orderModel: ReturnModelType<typeof Order>) { }

    public async getOne(id: string): Promise<Order> {
        return this.orderModel.findOne({ _id: id })
    }

    public async create(order: Order): Promise<Order> {
        return this.orderModel.create(order);
    }

    public async update(id: string, order: Order): Promise<Order> {
        return this.orderModel.findOneAndUpdate({ _id: id }, order);
    }
}