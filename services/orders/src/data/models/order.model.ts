import { modelOptions, prop } from "@typegoose/typegoose";
import { IsEnum, IsMongoId, IsOptional, IsString } from "class-validator";

import { StatusOrder } from "@app/shared/enum/statusOrder.enum";
 
@modelOptions({ schemaOptions: { versionKey: false } })
export class Order {
  @IsMongoId()
  @IsOptional()
  public _id?: string;

  @IsString()
  @prop({ required: true })
  public name: string

  @IsEnum(StatusOrder)  
  @IsOptional()
  @prop({ enum: StatusOrder, default: StatusOrder.CREATED })
  public status?: StatusOrder;
}