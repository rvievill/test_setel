
import { Module } from '@nestjs/common';
import { TypegooseModule } from "nestjs-typegoose";

import { Order } from "./models/order.model";
import { OrderRepository } from "./repositories/orders.repository";

@Module({
    imports: [
        TypegooseModule.forFeature([Order])
    ],
    controllers: [],
    providers: [OrderRepository],
    exports: [OrderRepository]
})
export class DataModule {}