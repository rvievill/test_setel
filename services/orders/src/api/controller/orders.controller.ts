import { Bind, Body, Controller, Get, HttpService, Param, Patch, Post } from "@nestjs/common";
import { OrderManager } from "@app/core/orders.manager";

import { Order } from "@app/data/models/order.model";
import { OrderRepository } from "@app/data/repositories/orders.repository";

@Controller("order")
export class OrderController {
    constructor(private orderRepository: OrderRepository, private orderManager: OrderManager, private http: HttpService) { }

    @Get(":id")
    @Bind(Param("id"))
    public getOne(id: string): Promise<Order> {
        return this.orderRepository.getOne(id);
    }

    @Post()
    @Bind(Body())
    public async createOrder(order: Order): Promise<Order> {
        let newOrder = await this.orderManager.create(order);
        const payment = (await this.http.post("http://payments:3001/payments", newOrder).toPromise()).data;
        newOrder = this.orderManager.updateStatus(payment, newOrder);
        newOrder = await this.orderManager.updateOrder(newOrder);

        return newOrder;
    }

    @Patch(":id")
    @Bind(Param("id"), Body())
    public update(id: string, order: Order): Promise<Order> {
        return this.orderRepository.update(id, order);
    }
}