
import { Module } from '@nestjs/common';
import { OrderController } from "./controller/orders.controller";
import { DataModule } from "../data/data.module";
import { CoreModule } from '../core/core.module';

@Module({
    imports: [
        DataModule,
        CoreModule
    ],
    controllers: [OrderController],
    providers: [],
})
export class ApiModule {}