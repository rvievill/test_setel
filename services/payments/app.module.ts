import { Module } from "@nestjs/common";
import { CoreModule } from "src/core/core.module";
import { ApiModule } from "src/api/api.module";


@Module({
    imports: [
        ApiModule,
        CoreModule
    ],
})
export class AppModule { }
