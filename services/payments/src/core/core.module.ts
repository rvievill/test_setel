import { Module } from '@nestjs/common';

import { PaymentManager } from './payments.manager';

@Module({
    controllers: [],
    providers: [PaymentManager],
    exports: [PaymentManager]
})
export class CoreModule {}