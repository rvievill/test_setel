import { HttpService, Injectable } from "@nestjs/common";

import { Order } from "@app/shared/interface/order.interface";

@Injectable()
export class PaymentManager {
    public processing(order: Order): string {
        const res = Math.floor(Math.random() * 100 % 2);

        return res === 1 ? "confirmed" : "declined";
    }
}