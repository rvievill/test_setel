export enum StatusOrder {
    CREATED = "created",
    COMFIRMED = "confirmed",
    DELIVERED = "delivered",
    CANCELLED = "cancelled"
}