import { StatusOrder } from "../enum/statusOrder.enum";

export interface Order {
    _id: string,
    name: string,
    status: StatusOrder
}