
import { Module } from '@nestjs/common';
import { PaymentController } from "./controller/payment.controller";
import { CoreModule } from '../core/core.module';

@Module({
    imports: [
        CoreModule
    ],
    controllers: [PaymentController],
    providers: [],
})
export class ApiModule {}