import { Bind, Body, Controller, Post } from "@nestjs/common";

import { PaymentManager } from "@app/core/payments.manager";
import { Order } from "@app/shared/interface/order.interface";

@Controller("payment")
export class PaymentController {
    constructor(private paymentManager: PaymentManager) {}

    @Post()
    @Bind(Body())
    public async processing(order: Order): Promise<string> {
        return this.paymentManager.processing(order);
    }
}